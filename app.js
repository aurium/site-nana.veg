"use strict";

const cacheKey = (new Date).toJSON().split(':')[0]

console.log('Loading Nana.Veg app files with cacheKey =', cacheKey)

import(`./palavras.js?${cacheKey}`)

;[...document.querySelectorAll('link[rel="stylesheet"]')].forEach(link => {
    link.href += '?' + cacheKey
})
