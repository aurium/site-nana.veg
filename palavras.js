"use strict";

const { abs, floor, sqrt } = Math
const rnd = (lim1=1, lim2=null)=> {
  if ( lim2 === null ) [lim1, lim2] = [0, lim1]
  return Math.random() * (lim2-lim1) + lim1
}
const rndI = (lim1, lim2)=> floor(rnd(lim1, lim2))
const body = document.body

const palavras = ['amigos', '+amor', 'amor', 'arroz', 'apaixonante', 'azeite', 'azeitonas', 'bahia', 'brócolis', 'casa', 'cenoura', 'coxinha', 'cogumelo', 'companhia', 'delícia', 'delivery', 'família', 'farinha', 'feijão', 'festa', 'fome!', 'gostosura', 'hummm...', 'juntinhos', 'leveza', 'liberdade', 'manjericão', 'marmita', 'molho', 'orégano', 'paladar', 'paz', 'pesto', 'pizza', 'quentinha', 'sabor', 'salgadinhos', 'salvador', 'tofu', 'tomate', 'vegan', 'vegequeijo']
    .sort(()=> (rnd()<.5) ? -1 : 1 )

palavras.forEach((p, i) => {
    //[document.createElement('div'), document.createElement('div')]
    [document.createElement('div')] // sem duplicadas
    .forEach((palavra, dup)=> {
        palavra.innerText = p
        palavra.className = 'palavra' + (dup ? ' palavra-duplicada' : '')
        body.appendChild(palavra)
        if ((i%2) === 0) {
             palavra.style.left = '105vw'
             palavra.side = 'right'
        } else {
             palavra.style.left = '-25vw'
             palavra.side = 'left'
        }
        palavra.style.top = rnd(-50, 139)+'vh'
        palavra.style.opacity = .3
        setTimeout(()=> movePalavra(palavra), (i+dup*palavras.length)*600)
    })
});

function movePalavra(palavra) {
    if (palavra.side === 'right') {
        palavra.style.left = rnd(-30, -25)+'vw'
        palavra.side = 'left'
    } else {
        palavra.style.left = rnd(105, 115)+'vw'
        palavra.side = 'right'
    }
    palavra.style.top = rnd(-50, 139)+'vh'
    palavra.style.opacity = 1
    setTimeout(()=> movePalavra(palavra), 26000)
}
